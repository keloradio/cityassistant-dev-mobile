//
//  Manager.swift
//  cityAssistant-dev
//
//  Created by Kelo Akalamudo on 2019-09-04.
//  Copyright © 2019 Radical. All rights reserved.
//

import UIKit
import Foundation
import SocketIO

class Manager  {
    
    static let sharedInstance = Manager()
    
    let chatBot = User(senderId: "000000", displayName: "City Assitant")
    var sender : User {
        return chatBot
    }
    
    let manager = SocketManager(socketURL: URL(string: SOCKET_SERVER)!, config: [.log(false), .compress, .path(SOCKET_PATH)])
    lazy var socket =  manager.defaultSocket
    private var onNewMessageCode: ((Message) -> Void)?
    
    let localeObject : [String : Any] = [
        "languageLocale" : "en_CA"
    ]
    
    var botReplyData : [Any]?
    
    var botChatMessage : ChatMessage?
    
    func initialize() {
        socket.on(clientEvent: .connect) { data, ack in
            self.socket.emit(SOCKET_CHANNEL, with: [self.localeObject])
        }
    }
    
    func addHandlers(){
        socket.on(SOCKET_CHANNEL) {[weak self] data, ack in
            self?.parseChatReply(data: data, completionHandler: { (message, error) in
                if let textAvailable =  message.text{
                    var queueMessage = Message(text: textAvailable, user: self!.sender, messageId: UUID().uuidString, date: Date())
                    self!.onNewMessageCode?(queueMessage)
                }
            })
        }
    }
    
    
    @discardableResult
    func onNewMessage(code: @escaping (Message) -> Void) -> Self {
           onNewMessageCode = code
           return self
    }
    
    func establishConnection() {
        socket.connect()
    }

    
    func parseChatReply(data: [Any], completionHandler: @escaping ((_ value: ChatMessage, _ error: Error?) -> Void)) {
        botReplyData = data
        let dataString = data[0] as! String
        do {
            let decoder = JSONDecoder()
            let resultingChatMessage = try decoder.decode(ChatMessage.self, from: dataString.data(using: .utf8)!)
            botChatMessage = resultingChatMessage
            completionHandler(resultingChatMessage,nil)
        } catch {
            print(error)
        }
    }

    func sendChatMessageHandlerWith(messageString: String) {
        botChatMessage?.text = messageString
        do {
            let encoder = JSONEncoder()
            let encodedData = try encoder.encode(botChatMessage)
            let encodedMessage = try JSONSerialization.jsonObject(with: encodedData, options: [])
            print(encodedMessage)
            self.socket.emit(SOCKET_CHANNEL, with: [encodedMessage])
        } catch {
            print(error)
        }
    }
}

