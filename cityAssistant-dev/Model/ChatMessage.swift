//
//  ChatMessage.swift
//  cityAssistant-dev
//
//  Created by Kelo Akalamudo on 2019-09-04.
//  Copyright © 2019 Radical. All rights reserved.
//

import Foundation

struct ChatMessage : Codable {
    var text: String?
    var sessionId : String?
    var sessionPath : String?
    var payload : Payload?
    var buttons : [Button]?
}

struct Payload : Codable {
    var simplicity: Simplicity
}

struct Simplicity : Codable {
    var type : String
    var text : [String]
    var buttons : [Button]?
}
struct Button : Codable {
    var value : String?
    var displayName : String?
}

/*
 botChatMessage::::::::::::::::::::Optional(cityAssistant_dev.ChatMessage(text: Optional("Greetings!, I’m CityAssistant bot. You can choose a topic below to get started, or type a question. What can I help you with?"), sessionId: Optional("9ba60824-ec54-47e6-8786-51bc426efcfb"), sessionPath: Optional("projects/simplicity-city-assistant-i-ug/agent/sessions/9ba60824-ec54-47e6-8786-51bc426efcfb"), payload: Optional(cityAssistant_dev.Payload(simplicity: cityAssistant_dev.Simplicity(type: "onboarding", text: ["Hiya!", "I’m CityAssistant bot. You can choose a topic below to get started, or type a question.", "What can I help you with?"], buttons: Optional([cityAssistant_dev.Button(value: Optional("facilities"), displayName: Optional("Facilities")), cityAssistant_dev.Button(value: Optional("report issue"), displayName: Optional("Report an Issue")), cityAssistant_dev.Button(value: Optional("property tax"), displayName: Optional("Property Tax Fees"))])))), buttons: nil))

 */
