//
//  User.swift
//  cityAssistant-dev
//
//  Created by Kelo Akalamudo on 2019-09-03.
//  Copyright © 2019 Radical. All rights reserved.
//

import Foundation
import MessageKit

struct User : SenderType, Equatable {
    var senderId: String
    var displayName: String
}


